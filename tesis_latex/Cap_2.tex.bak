\chapter{Marco Te�rico}

\section{Visi�n General}

En procesamiento de im�genes, las t�cnicas usadas para descartar e identificar ciertas areas de inter�s (\textit{Regions of Interest} - ROI) de la imagen son llamadas t�cnicas de segmentaci�n, un proceso similar realizan los m�todos de sustracci�n de fondo, con la diferencia que la sustracci�n de fondo tiene que considerar la historia de un determinado objeto en los \textit{frames} anteriores. Intr�nsecamente, los algoritmos de segmentaci�n son complejos por lidiar con diferentes problemas: la calidad del dispositivo de captura, formato de compresi�n de la imagen (el formato JPG usa compresi�n con perdida de informaci�n), propiedades intr�nsecas de la imagen (brillo, contraste), oclusi�n de objetos o mal enfoque del dispositivo de captura \cite{gonzalez}. Puesto que, una secuencia de im�genes constituye un v�deo dando la sensaci�n de movimiento. Entonces, el procesamiento de video, tiene ademas de los problemas de procesamiento de im�genes otros problemas como la compresi�n adicional de los formatos de video, el desenfoque causado por objetos brillosos en el video, y fundamentalmente los algoritmos deben funcionar en tiempo real para no desfasar los resultados \cite{ref3}. Es decir, tenemos que lidiar con el \textit{frame rate} (la cantidad de frames utilizados para generar una secuencia de video de 1 segundo), el \textit{frame rate} mas utilizado por los formatos de videos (codecs) es de 27 \textit{frames} por segundo. Esto quiere decir que los algoritmos que procesen videos tienen que estar en la capacidad de realizar todas las etapas del procesamiento de video con por lo menos 27 \textit{frames} por segundo, sin dejar de lado la precision del sistema, siendo la etapa de sustracci�n de fondo la mas critica por trabajar directamente con todo el frame.

\section{Pre-Procesamiento}

El procesamiento digital de im�genes esta definido como un conjunto de t�cnicas que se aplican a los \textit{frames} con el objetivo de mejorar la calidad, eliminar el ruido y resaltar la informaci�n importante para posteriores etapas \cite{sardi} . A continuaci�n se detallara algunas de esas t�cnicas:

\subsection{Filtro Gausioano}

El filtro gaussiano \cite{gaussian} suaviza la imagen y eliminar el ruido. Consiste en la mezcla de los colores de una imagen para conseguir un efecto de desenfoque. La funci�n de la distribuci�n Gaussiana ayuda a adjudicar cuanto aporta cada \textit{pixel}, en funci�n de lo cerca o lejos que se encuentre. Esta funci�n tiene la forma $f(x)=ae^{(-(x-b)^2/c^2)} $ donde $(x-b)$ es la distancia entre el \textit{pixel} que esta siendo modificado y el \textit{pixel} del que se esta tomando el valor, $a$ y $c$ valen 1, y $f(x)$ resulta un numero real entre 0 y 1 que representa el porcentaje de influencia. Cuando es aplicado a una imagen puede ser graficado como se muestra en la Figura \ref{fig:gaussiano}.

\begin{figure}[bht!]
\centering
	\subfloat[Imagen de entrada]{
        \label{fig:gauss1}         %% Etiqueta para la primera subfigura
        \includegraphics[width=0.20\textwidth]{graficos/imagenes/3_gaussian_1.jpg}}    
	\subfloat[Resultado de aplicar el filtro Gaussiano a la imagen (a)]{
        \label{fig:gauss2}         %% Etiqueta para la primera subfigura
        \includegraphics[width=0.20\textwidth]{graficos/imagenes/3_gaussian_2.jpg}}
\caption{El filtro Gaussiano sirve para dar el efecto borroso a una imagen y para eliminar el ruido de la imagen.}
  \label{fig:gaussiano}
\end{figure}


\subsection{Dilataci�n}
La operaci�n morfol�gica de dilataci�n \cite{gonzalez} es usada para eliminar el ruido que puede existir entre los bordes de los objetos, da el efecto de engrosar dichos objetos de la imagen tal como lo muestra la Figura \ref{fig:dilate}.
Formalmente, la dilataci�n $A \oplus B$ es el conjunto de puntos de todas las posibles sumas de pares de elementos, uno de cada conjunto $A$ y $B$. Dados $A$ y $B$ como conjuntos en $Z^2$, la dilataci�n de $A$ por $B$ se denota de la siguiente Ecuaci�n:
\begin{equation}
A \oplus B = \{z | (B)_z  \cap A \neq 0 \}
\end{equation}
Esta ecuaci�n esta basada en obtener la reflexi�n de $B$ sobre su origen y alternar esta reflexi�n por $z$. La dilataci�n de $A$ por $B$ es entonces el conjunto de todos los desplazamientos $z$, tal que $A$ y $B$ sobresalen por al menos un elemento.

%%%%%imagen
\subsubsection{Erosi�n}
La operaci�n morfol�gica de erosi�n \cite{gonzalez} es usada para eliminar el ruido que puede existir entre los bordes de los objetos, da el efecto de adelgazar dichos objetos de la imagen tal como lo muestra la Figura \ref{fig:erode}.
Formalmente el operador de erosi�n combina dos conjuntos usando la resta vectorial y es el dual de la dilataci�n. Dados $A$ y $B$ en $Z^2$, la erosi�n de  $A \ominus B$ se denota como muestra la siguiente Ecuaci�n.
\begin{equation}
	A \ominus B = \{z|(B)_z \subseteq A \}
\end{equation}
Esta ecuaci�n indica que la erosi�n de $A$ por $B$ es el conjunto de todos los puntos $z$ tal que $B$, trasladado en $z$, esta contenido en $A$. El resultado de la  son aquellos puntos de A para los cuales todas las posibles traslaciones definidas por $B$ tambi�n est�n en $A$.
%%%%%%%figura
\begin{figure}[bth!]
  \centering
    \includegraphics[width=8cm]{graficos/imagenes/4_dilation.jpg}
  \caption{La operaci�n morfol�gica de dilataci�n sirve para eliminar el ruido, engrosar la imagen y juntar objetos. En esta figura se muestra a la imagen A y el operador B, tal que el resultado $A \oplus B$ es una imagen mas gruesa, cabe resaltar que la parte izquierda de la imagen A fue rellenada dando el efecto de llenar espacios. Imagen extra�da de \cite{gonzalez}}
  \label{fig:dilate}
\end{figure}
\begin{figure}[bth!]
  \centering
    \includegraphics[width=8cm]{graficos/imagenes/5_erosion.jpg}
  \caption{La operaci�n morfol�gica de erosi�n sirve para eliminar el ruido, adelgazar la imagen, y separar objetos. En esta figura se muestra la imagen A y el operador B, tal que el resultado $A \ominus B$ es una imagen mas delgada, cabe resaltar que la parte sobresaliente derecha de la imagen A fue cortada por el operador de erosi�n, llegando a reducir impurezas en la imagen. Imagen extra�da de \cite{gonzalez}}
  \label{fig:erode}
\end{figure}



\section{Sustracci�n de Fondo}

El objetivo principal de la presente tesis es realizar la detecci�n autom�tica de personas en secuencias de video, por lo cual, se tendr� que analizar cada frame de los videos de entrada. Lo cual representar�a un procesamiento de alto desempe�o debido a la gran cantidad de informaci�n entrante, entonces, es necesario utilizar algoritmos que puedan descartar determinadas areas que sean poco relevantes para el   an�lisis de posteriores etapas. Es as� que los Algoritmos de Substraccion de Fondo, que por medio de la diferencia matem�tica o por an�lisis estad�sticos, tienen como principal objetivo obtener las �reas que presenten movimientos por medio del an�lisis de \textit{frames}, generando �reas peque�as que servir�n de entradas para etapas posteriores. La principal ventaja de los m�todos de sustraccion de fondo para la detecci�n de personas es que descartar enormes areas que tienen una baja o nula probabilidad de contener una persona.

\subsection{Problem�tica}
Son diversas las dificultades para detectar candidatos iniciales en la escena causados por la presencia de bordes, m�ltiples texturas, cambios bruscos de iluminaci�n, sombras, reflexiones y cualquier tipo de variaci�n de fondo. Los factores cr�ticos que se han identificado son:

\begin{description}
\item[Texturas complejas:] Los escenarios incluyen una cantidad importante de objetos con diversas texturas que pueden dificultar la localizaci�n de areas de movimiento. Dependiendo del algoritmo utilizado, determinadas areas (como el movimiento de objetos provocado por el viento) pueden ser f�cilmente detectados err�neamente como areas de movimiento que no son parte del objetivo del sistema.

\item[La Variabilidad de factores externos:] Diversos factores externos pueden afectar al fondo de la imagen (\textit{background}), como la luz y el angulo de visi�n de la c�mara, o objetos brillosos (como el agua, espejos, vidrios, etc). 
Y se definen principalmente los siguientes:

\begin{itemize}
\item Sombras: Formalmente las sombras tambi�n representan �reas de movimiento, pero no deben ser detectadas por ser consideradas ruido. 
\item Movimiento: Las c�maras para el an�lisis de video est�n colocadas normalmente en un angulo fijo, muchos algoritmos de detecci�n de movimiento toman como ventaja este aspecto para dar r�pidamente los resultados por comparar cada nuevo frame con un \textit{background} est�tico. Algoritmos adaptativos al movimiento suelen ser mas complejos y pueden ser implementados si se sabe que la c�mara cambiar de angulo o si existe mucha variaci�n de factores externos.
\item Iluminaci�n: Tal como se menciona en el item anterior, muchos algoritmos usan un \textit{background} est�tico para su procesamiento, esto no generar�a buenos resultados si se requiere que el sistema funcione tanto con la luz del d�a, como con la luz de la tarde o incluso en la noche.
\end{itemize}
\end{description}

\subsection{Tipos de Sustracci�n de Fondo}

Los m�todos de sustracci�n de fondo est�n clasificados en dos grupos:
\begin{description}
\item[Con fondo est�tico:]
Son computacionalmente baratos por requerir poco procesamiento. Reciben como entrada inicial una imagen (\textit{background}) que servir como referencia  para cada uno de los \textit{frames} del video. Es decir, la imagen de entrada inicial es una imagen que no debe contener objetos que provoquen movimiento alguno, ya que, esta imagen sera comparada con cada uno de los \textit{frames} y seg�n un determinado valor de umbral se determinar si hubo o no un movimiento en dicha zona como se muestra en la Figura \ref{fig:estatico}. La desventaja de este tipo de m�todos es que requieren que la c�mara este totalmente fija, basta tan solo que la c�mara se mueva un poco para que el m�todo de sustracci�n de fondo falle.

\begin{figure}[bht!]
	  \centering
		\includegraphics[width=8cm]{graficos/imagenes/1_fondostatico.pdf}	
	  \caption{Los m�todos basados en fondo estatico realizan la diferencia de \textit{frames} entre el \textit{Background}(a) y un \textit{Frame} (b) para obtener las �reas de movimiento (c).}
	  \label{fig:estatico}
\end{figure}

\item[Con fondo din�mico:]
Son computacionalmente mas costosos que los m�todos basados en fondo est�tico, esto debido a que usan comparaciones estad�sticas. Algunos de estos nematodos requieren un tiempo de entrenamiento de los \textit{frames} para obtener el \textit{background} din�mico que se ira actualizando constantemente. As�, el resultado entre la diferencia del \textit{frame} y el \textit{background} din�mico es mostrado en la Figura \ref{fig:dinamico}. Es bastante �til para el desempe�o en lugares a campo abierto, donde incidencias de luz, tales como el sol, pueden cambiar constantemente la intensidad del reflejo de los colores de cada uno de los objetos. Incluso son independientes al movimiento de las c�maras.

\begin{figure}[bht!]
	  \centering
		\includegraphics[width=8cm]{graficos/imagenes/2_fondodinamico.pdf}	
	  \caption{Los \textit{frames} A, B, C y D sirven para realizar un an�lisis estad�stico y generar un background din�mico como se muestra en (a). Posteriormente cada frame sera comparado con el \textit{background} din�mico anteriormente generado, y as� detectar las �reas de movimiento como se muestra en (b).}
	  \label{fig:dinamico}
\end{figure}
\end{description}

\section{T�cnicas de Sustracci�n de Fondo}

La sustracci�n de fondo es ampliamente explorada en sistemas de procesamiento de video por analizar directamente toda la informaci�n de cada uno de los \textit{frames} para encontrar �reas en movimiento. Esto quiere decir que esta etapa es critica, ya que, se deben procesar de forma r�pida aproximadamente 27 \textit{frames} (\textit{frame rate}) con la mayor precisi�n posible. Pero tambi�n esta etapa es considerada importante para un sistema de video vigilancia inteligente por las siguientes razones:

\begin{itemize}
	\item Consigue separar los objetos que presentaron un movimiento significativo en el video.
	\item Por medio del pre-procesamiento consigue descartar objetos peque�os (ruido) que no son relevantes para el an�lisis.
	\item Facilita a las siguientes etapas de los sistemas de video vigilancia porque les indica en que �reas analizar.
\end{itemize}

Entre los principales m�todos de sustracci�n de fondo tenemos: Diferencia de \textit{frames} \cite{ref3}, 
\textit{Speed Up Temporal Median Filter} (SU-TMF) \cite{medianfilter},
\textit{Mixture of Gaussians} (MoG) \cite{mog}, 
Sustracci�n de fondo basada en acumulaci�n de pesos \cite{unger},
\textit{Running Gaussian Average} (GA) \cite{wren},
\textit{Kernel Density Estimation} (KDE) \cite{Elgammal},
\textit{Sequential Kernel Denisty Approximation} (SKDE) \cite{Bohyung}, entre otros. A continuaci�n se detallaran las t�cnicas de sustracci�n de fondo mas resultantes.

\subsection{Diferencia de frames}
\label{subsub:difframes}
Este m�todo es del tipo de m�todos de sustracci�n de fondo que usa un fondo est�tico \cite{ref3}. Quiere decir, su procesamiento es computacionalmente barato. Pero tiene ciertas restricciones, requiere una imagen de entrada (\textit{Background}) que no debe contener ning�n objeto en movimiento. Y ademas, solo puede ser usada cuando la c�mara esta fija, es decir, basta que la c�mara se mueva tan solo un poco de su foco para que la detecci�n deje de funcionar. Formalmente, esta representada por la siguiente Ecuaci�n:
\begin{equation}
\label{eq:diffframes}
d(i,j) = \begin{cases} 0, &\mbox{ si } |f_1(i,j) - f_2(i,j)| \leq T \\ 
1, &\mbox{ en otro caso.} 
\end{cases}
\end{equation}
donde $f_2$ es el \textit{Background}, $f_1$ el frame actual, y $T$ un umbral. El prop�sito del umbral $T$, es reducir el ruido y los cambios en la iluminaci�n de dicha escena.

\subsection{Temporal Median Filter (TMF)}
\label{sec:tmf}
La t�cnica propuesta en \cite{unger}, es del tipo de m�todos de sustracci�n de fondo din�mico. Es por eso que recibe �nicamente los \textit{frames} del video para analizarlos individualmente. Este m�todo retornara mejores resultados con un numero mayor de im�genes. Formalmente la sustracci�n de fondo basada en acumulaci�n de pesos, esta representado por la Ecuaci�n \ref{eq:acc}.

\begin{equation}
\left( \begin{matrix} r_t \\  g_t \\  b_t  \end{matrix} \right)  = \sum_{i=1}^{t} \omega_i * \left( \begin{matrix} r_i \\  g_i \\  b_i  \end{matrix}  \right)
\label{eq:acc}
\end{equation}
Donde $(r,g,b)$ indican los 3 canales de la imagen en el espacio de colores RGB, $t$ indica la cantidad de im�genes a considerar para obtener el \textit{background} din�mico, y $\omega_i$ indica el peso que se le otorga a la imagen en la posici�n $i$. El autor sugiere dar mayor peso a las primeras im�genes y menor peso a las ultimas, esto para evitar trastornos en los videos. La Figura \ref{fig:backgrounddin1} muestra el resultado de aplicar este algoritmo con tan solo 50 iteraciones, osea $t=50$, en esta imagen aun se pueden ver rastros de personas. Por otro lado, la Figura \ref{fig:backgrounddin2} muestra el resultado de aplicar el algoritmo con 300 iteraciones, osea $t=300$.

\begin{figure}[bht!]
\centering
	\subfloat[Resultado con $t=50$]{
        \label{fig:backgrounddin1}         %% Etiqueta para la primera subfigura
        \includegraphics[width=0.30\textwidth]{graficos/images/5_background_dinamico.jpg}}    
	\subfloat[Resultado con $t=300$]{
        \label{fig:backgrounddin2}         %% Etiqueta para la primera subfigura
        \includegraphics[width=0.30\textwidth]{graficos/images/0_background.jpeg}}
\caption{El m�todo de acumulaci�n de pesos esta basada en la asignaci�n de pesos a cada \textit{frame} para obtener el \textit{background} din�mico. (a) El resultado presenta rastros de movimientos recientes usando $t=50$, (b) el resultado se muestra libre de objetos en movimiento debido a usar 300 iteraciones, $t=300$. }
  \label{fig:accweigh}
\end{figure}


\subsubsection{Speed Up Temporal Median Filter (SUTMF)}
\label{subsub:SUTMF}
El m�todo \textit{Speed Up Temporal Median Filter} (SU-TMF) \cite{medianfilter} es una optimizacion del m�todo \textit{Temporal Medial Filter} (TMF) \cite{ching}, para entender detalladamente este m�todo se explicara primeramente el m�todo TMF. 
El m�todo TMF ha sido ampliamente usado por diferentes tipos de sistemas con la misma finalidad, detectar �reas en movimiento. El TMF ha mostrado buenos resultados debido a su \textit{background} din�mico actualizado constantemente, esto hace que sea adecuado para el uso en sistemas de vigilancia en exteriores (parques, calles o cualquier campo abierto). El m�todo TMF esta representado matem�ticamente por la siguiente Ecuaci�n:
\begin{equation}
B(x,y,t) = \dfrac{1}{N} \sum_{i=0}^{N-1} I(x,y,t-i)
\label{eq:tmf}
\end{equation}
donde $(x,y)$ representa la posici�n del \textit{pixel} en la imagen, $N$ indica el numero de im�genes a considerar para extraer el \textit{background} din�mico, mientras que $t=\{1,2, \dots , N \}$. El resultado de la Ecuaci�n \ref{eq:tmf} es un \textit{background} para el tiempo $t$.
Sin embargo, el costo computacional del TMF puede ir creciendo exponencialmente seg�n el tama�o de la imagen y la cantidad de iteraciones. As� surge m�todo \textit{Speed Up Temporal Median Filter} (SU-TMF) para mejorar al m�todo TMF en tiempo de respuesta, para que pueda trabajar en sistemas de tiempo real. El m�todo SU-TMF no considera todos los \textit{frames} consecutivos para obtener un fondo din�mico, por lo contrario, escoge ciertos \textit{frames} para poder determinar posibles �reas de movimiento. Los \textit{frames} consecutivos presentan medianas similares, representando informaci�n irrelevante para la determinaci�n r�pida de un \textit{background} din�mico, es por eso que pueden ser descartados. El m�todo SU-TMF selecciona a determinados \textit{frames} usando una selecci�n basada en histogramas.

\begin{description}
\item[Median Selection based on Histogram:]
\label{sec:msbhist}
Define la forma de selecci�n de \textit{frames} relevantes para el m�todo SU-TMF. Este m�todo de selecci�n \cite{medianfilter} esta basado en histogramas, formalmente definida por la Ecuaci�n \ref{eq:medhistogram}.

\begin{equation}
D_k = \{d_{k-1}, d_{k-2}, \dots , d_{k-N}\}
\label{eq:medhistogram}
\end{equation}

donde $D_k$ representa la vecindad de los $N$ vecinos del \textit{pixel} $(x,y)$. Definimos $O_{Mid} = (N-1)/2$ donde N debe ser impar. Entonces, los \textit{frames} seleccionados ser�n aquellos donde los $d_k$ sean mayores a $O_{Mid}$, con la restricci�n de estar en el rango siguiente, $lb \leq O_{Mid} \leq ub$, donde $lb$ y $ub$ son los limites inferior y superior respectivamente.\vspace*{0.3cm}

Por lo tanto, el m�todo SU-TMF utiliza los \textit{frames} seleccionados por el \textit{Median Selection based on Histogram} para utilizar la ecuaci�n \ref{eq:tmf} con pocos \textit{frames} a comparaci�n del m�todo TMF.
Mientras que el m�todo TMF requiere de muchos \textit{frames} para poder obtener buenos resultados, el m�todo SU-TMF requiere de una cantidad menor de \textit{frames} torn�ndolo computacionalmente mas barato. La obtenci�n de las �reas de movimiento se realiza como muestra la siguiente Ecuaci�n:
\begin{equation}
\left| I(x,y,t) - \dfrac{1}{n} \sum_{i=0}^{n-1} I(x,y,t-i) \right|   > T
\end{equation}

\end{description}

\subsection{Mixture of Gaussians (MoG)}

El m�todo Mixture of Gaussians (MoG) \cite{mog} es probablemente uno de los m�todos de fondo din�mico mas usados por su versatilidad y precision para descartar sombras, lo cual no es considerado por los otros m�todos anteriormente citados. Entre otras ventajas, el m�todo MoG es adaptativo a cambios de brillo, saturacion, movimiento de c�mara, etc. El metodo MoG mantiene una funci�n de densidad por cada \textit{pixel}, as�, es capaz de manejar distribuciones de background din�mico. Por otro lado, el modelo de parametros del m�todo MoG pueden ser actualizados adaptativamente sin analizar muchos \textit{frames} del video.
La distribuci�n de pixeles $f(I_t = u)$ esta modelado como una mezcla de K Gaussianas como muestra la Ecuaci�n \ref{eq:mog1}.

\begin{equation}
\label{eq:mog1}
f(I_t = u) = \sum_{i=1}^{k}   \omega_{(i,t)} \eta ( u, \mu_{(i,t)} , \sigma_{(i,t)}) 
\end{equation}

donde $\eta ( u, \mu_{(i,t)} , \sigma_{(i,t)})$  es la i-esima componente Gaussiana con intensidad media $u, \mu_{(i,t)}$ y $u, \mu_{(i,t)}$ es la desviaci�n est�ndar, $omega_{(i,t)}$ es la porci�n de datos representado por el i-esimo componente.
Normalmente K toma valores de entre 3 y 7, depende mucho de la complejidad de la imagen.

Las m�ltiples superficies que puedan aparecer en un \textit{pixel} son modeladas por las mezclas de las Gaussianas. Debido a su procesamiento adaptativo reduce la detecci�n de sombras autom�ticamente. En cada iteracion, las Gaussianas son evaluadas usando una heuristica simple, para determinar la mas adecuada y ver que corresponda al \textit{Background}. Aquellos \textit{pixeles} que no correspondan al Background de Gaussianas ser�n clasificados como pixeles pertenecientes al primer plano (objetos en movimiento) o lineas negras como se muestra en la Figura \ref{fig:mog_fig}.

\begin{figure}[bht!]
	  \centering
		\includegraphics[width=8cm]{graficos/imagenes/6_MoG_2.jpg}	
	  \caption{El Modelo de Gaussianas Mixtas (MoG) puede diferenciar del background y de el \textit{foreground} por medio del modelado de Gaussianas, en este caso la figura muestra a las lineas rojas como parte del background. Imagen extra�da de \cite{mog}}
	  \label{fig:mog_fig}
\end{figure}

Las gaussianas est�n ordenadas por el valor de $\omega/\sigma$ que indican que el alto soporte y poca variaci�n tendr�n altos valores. As�, simplificamos las primeras B distribuciones que fueron escogidas del background modelo:

\begin{equation}
\label{eq:mog1}
B = argmin_b(\sum_{i=1}^{b} \omega_{i}> T ) 
\end{equation}

donde T es la porci�n m�nima de la imagen que se espera sea del background.

\subsection{Optical Flow}
\label{sub:opticalflow}
Tambi�n conocido como Flujo Optico \cite{ref8}, trabaja como un campo de velocidad que deforma una imagen en otra y describe el movimiento del punto o caracter�stica entre im�genes. En \cite{CinbisS12} se plantea una mejora de este m�todo, pues plantea una secuencia de vectores que hacen una comparaci�n entre \textit{frames} para determinar el movimiento de una determinada qrea o pixel, y por eso genera una gr�fica con vectores.

El flujo �ptico esta definido como muestra la Ecuaci�n \ref{eq:opticalflow}.
\begin{equation}
I(x,y,t) = I(x + \bigtriangleup x, y + \bigtriangleup y , t +\bigtriangleup t)
\label{eq:opticalflow}
\end{equation}

Donde $(x,y)$ representa al \textit{pixel}, $t$ representa a la imagen en el tiempo $t$, y el operador $\bigtriangleup$ indica variaci�n. A continuaci�n detallaremos los principales problemas con los cuales tiene que lidiar el \textit{Optical Flow}:

\begin{itemize}
	\item Errores y Ruido: El ruido es representado por vectores con magnitud peque�a que pueden ser descartados por un determinado umbral VL. Los errores son todo lo contrario, porque ser vectores de magnitudes grandes.
%Los errores pueden tambi�n ser controlados con un umbral VH. 
	\item Bajo desempe�o discriminatorio: Debido a que el nivel de clasificaci�n para distinguir clases/objetos es baja. 
\end{itemize}

Posteriormente se procede a la descripci�n del movimiento, que consiste en extraer caracter�sticas de los m�todos de an�lisis de movimiento y posteriormente la etapa de extracci�n de caracter�sticas, tal como se puede observar en la Figura \ref{fig:opticalflow}.

\begin{figure}[bht!]
  \centering
    \includegraphics[width=6cm]{graficos/imagenes/7_optical_flow.jpg}
  \caption{Ejemplo de flujo �ptico, donde las lineas azules indican que hubo movimiento entre \textit{frames}. Incluso, la dimension de los vectores mostrados indica la intensidad del movimiento. Imagen extra�da de \cite{CinbisS12}.}
  \label{fig:opticalflow}
\end{figure}

En concreto, el flujo �ptico define los vectores de movimiento de diferentes partes de la imagen.
Cabe resaltar que el flujo �ptico no hace espec�ficamente sustracci�n de fondo, pero cumple la misma funci�n que los m�todos de sustracci�n, es decir, detecta las �reas de movimiento, e incluso hace parte del seguimiento de objetos (\textit{tracking}).

\section{Detecci�n de Personas}
\label{sec:deteccion}

La detecci�n de personas tiene como funci�n principal detectar personas en diferentes escenarios, as� como descartar objetos que no representen/contengan una persona. Estos m�todos necesitan de t�cnicas de pre-procesamiento para poder trabajar en tiempo real, ya que, si recibiesen los \textit{frames} directamente de la video c�mara, el tiempo de respuesta seria alto. Provocando as� retrasos en la detecci�n, es por eso que los m�todos de Sustracci�n de fondo calculan previamente las �reas que hayan presentado movimientos entre \textit{frames}. As�, enormes �reas de cada frame son descartados y los m�todos de Detecci�n de personas trabajan sobre sub-im�genes.

La detecci�n de personas tiene como entrada, �reas de movimiento generadas por m�todos de sustracci�n de fondo, y tienen como principal etapa definir por medio de eucar�sticas y m�todos formales si determinada �rea de movimiento fue generada por una persona o no.

Existen diferentes abordajes para la detecci�n de personas. Los \textit{keypoints} son los m�s utilizados para representar los puntos de interes de cada persona, as�, extrayendo caracter�stica locales podr�n ser identificadas por una t�cnica de Inteligencia Artificial. Otro tipo de m�todos de detecci�n de personas se basa en la aproximaci�n de la posici�n de las extremidades, considerando que el costo computacional suele ser m�s elevado. Estos �ltimos sirven tambi�n para hacer proyecciones tridimensionales o aproximaciones a representaciones MOCAP.

\begin{figure}[bht!]
  \centering
    \includegraphics[width=8cm]{graficos/mocap1.jpg}
  \caption{Algunos sistemas MOCAPs autom�ticos se basan en la detecci�n de las extremidades de las personas. Normalmente es realizado por medio de sensores de posici�n de puntos fijos en la persona para mayor precision.}
  \label{fig:mocap}
\end{figure}

Mientras que otro tipo de Detectores de personas, representados por el m�todo Histogram of Oriented Gradient (HOG) crean un unico patr�n para varias personas, lo cual permite hacer una detecci�n m�s r�pida usando gradientes de intensidad de grises.

\subsection{Problem�tica}
Se relaciona con el numero de objetos, su velocidad, oclusiones parciales, suponiendo variaciones e iteraciones. Tenemos dos factores:
\begin{description}
\item[Apariencia de Variabilidad:] Objetos no r�gidos, diferentes tipos de ropa, tama?o y forma.
\item[Interacci�n de Personas y Objetos:] Las personas deben ser detectadas  en el entorno que los rodea. As� se presentan interacciones con objetos y personas surgiendo el problema de las oclusiones que limitan la apariencia visible de la persona ocluida.
\end{description}

\section{T�cnicas de Detecci�n de Personas}

Para algoritmos de detecci�n de personas existen muchas t�cnicas pero se pueden agrupar en dos grupos uno que busca detectar e identificar partes de la persona para decir que es persona y otro que basa sus respuestas en m�todos estad�sticos.

\subsection{Histograms of Oriented Gradients (HOG)}

HOG (Histogram of Oriented Gradients - Histograma de Gradientes Orientados) es un tipo de extractor de caracter�sticas. Intenta generalizar un mismo objeto de una clase en diferentes angulos y condiciones, en este caso una persona. Este m�todo genera un �nico vector de caracter�sticas por cada persona, previo entrenamiento en diferentes �ngulos. Normalmente otros m�todos de detecci�n de personas son basadas en caracter�sticas locales obtenidas por puntos principales (\textit{Key points}) de la persona, lo cual dificulta el proceso de detecci�n. En cambio, un unico vector torna el trabajo de detecci�n m�s sencillo. 

El m�todo re-escala la imagen de entrada hacia 64x128 pixeles para conseguir la independencia de la escala. El m�todo divide la imagen en peque?as celdas, los autores sugieren utilizar una celda de 8x8. Cada una de estas celdas es multiplicada por cada celda correspondiente en la imagen en an�lisis, as� se obtendr� una malla como se puede ver en la Figure \ref{fig:hogtranf}. A�n esta imagen puede ser procesada para obtener mejores resultados con bases radiales para resaltar m�s los bordes de la persona. Debido a usar una celda de 8x8, tendremos 64 gradientes, los cuales seran proyectados en un histograma de 9 \textit{bins}. El histograma es utilizado para cuantizar la informaci�n de la celda y asi reducir una informacionde 64 valores por tan solo 9 (magnitud de cada \textit{bin}). Este proceso ayuda al desempe�o del clasificador. Posteriormente, el vector de 9 \textit{bins} es normalizado para lograr la invarianza a la escala. 

Posteriormente cada Bloque (2x2 celdas contiguas que forman un cuadrado) ser� normalizado. La normalizacion de cada Bloque se consigue concatenando los valores de las 4 celdas en un unico bloque de 36 componentes (4 celdas de histogramas con 9 \textit{bins} cada uno). Dividir el vector por la magnitud del vector para normalizarla.

Por cada imagen de 64x128, se obtendran 7 bloques horizontales y 15 bloques verticales, dando un total de 105 bloques. Cada bloque contiene 4 celdas con 9 \textit{bins} de histograma. Lo cual genera un vector de 3780 valores (7*15*4*9). Esta informaci�n servir� de vector de caracter�sticas para una Support Vector Machine (SVM).

\begin{figure}[bht!]
\centering
	\subfloat[Divisi�n]{
        \label{fig:hogdivision}         %% Etiqueta para la primera subfigura
        \includegraphics[width=0.25\textwidth]{graficos/hog.png}}    
	\subfloat[Gradiente]{
        \label{fig:hogtranf}         %% Etiqueta para la primera subfigura
        \includegraphics[width=0.25\textwidth]{graficos/hog2.jpg}}
\caption{(a)Division de una imagen en celdas (lineas negras) y bloques (rojo), (b) Visualizaci�n de los histogramas da orientaci�n de la gradiente por c�lulas, antes de ser agrupado por bloque y normalizado}
  \label{fig:hog1}
\end{figure}

El m�todo \cite{DT05} se divide en cuatro fases: el calculo de la magnitud y la orientaci�n de los bordes de la imagen, dividir la imagen en bloques y c�lulas, el calculo del histograma de gradientes por las c�lulas agrupadas en bloques, la concatenaci�n de estos histogramas. Un vector descriptor es entonces la concatenaci�n de los distintos histogramas. La primera fase, el calculo de la direcci�n y magnitud de los bordes se lleva a cabo de acuerdo con los m�todos de detecci�n de bordes como Prewitt, Filtros de Canny y Sobel a una dimension. En la segunda fase se consideran dos estructuras, llamadas celdas y bloques. Estas celdas establecen en la imagen original con unas dimensiones de $vxv$ p?xeles. La \ref{fig:hogdivision} ilustra la division de una imagen en celulas. Un bloque es visto como una articulaci�n $ncnc$ celdas, para un total de $(ncv)(ncv)$ p?xeles. Esta divisi�n en las celdas y los bloques tratar de maximizar la ganancia de informaci�n de la evaluaci�n de una imagen, lo que permite tener algunos descriptores estad�sticos asociados no solo con la observaci�n directa del color o la magnitud de un pixel. Tambi�n en la figura \ref{fig:hogdivision} se observa grupos de celdas en bloques.

La tercera fase, la mas complicada, consiste en agrupar las direcciones de los bordes en un histograma con divisiones del espacio de [-90, 90] para la orientaci�n. Esta agrupaci�n se realiza mediante uno de los bloques de celdas, repitiendo este proceso para los bloques restantes.
En la cuarta etapa, el histograma de las direcciones de gradiente se calculara por bloque, cada celda por separado, es decir, un total de $n2c$ histogramas por bloques. Considere un p?xel px(i,j), ck celda de un bloque dado, sea la magnitud del gradiente del p?xel dado por s(i,j) y su orientacion por L = 1, 2,  ?u  ?u  ?u, nb. Este p?xel contribuir� para la orientaci�n del histograma de la celdas ck con s(i,j) como valor de su magnitud. Este proceso se repite para los p?xeles restantes de la celda, siendo el resultado un vector de $n$ dimensiones.

\begin{figure}[bht!]
  \centering
    \includegraphics[width=8cm]{graficos/bloque.png}
  \caption{Recorrido de los bloques usando 2x2 celdas.}
  \label{fig:mocap}
\end{figure}

Por ultimo, se realiza una normalizacion del histograma de forma que puede ser visto como una estimativa de una funci�n de densidad de probabilidades. Este proceso se repite para cada uno de las celdas del bloque y el resultado de todas estas c�lulas concatenadas. Este resultado se conoce como descriptor de HOG.

\subsection{Pictorial Structures Revisited}


Esta tecnica \cite{AndrilukaRS09} propone identificar el movimiento individual de cada una de las partes del cuerpo humano como elementos �nicos, considerando inicialmente las extremidades como puntos focales. Usando un m�todo estimativo de detecci�n de posiciones de personas. La cantidad de partes a detectar puede variar, pero es recomendable detectar 6 partes diferentes como: cabeza, tronco, y las 4 extremidades, pero  considera 4 partes inferiores del cuerpo: piernas superiores e inferiores izquierdo y derechos, resultando modelos de 10 partes.
Esta propuesta usa un modelo cinem�tico que establece restricciones de los movimientos de las partes del cuerpo en base a probabilidades. Usa una estructura DAG (Grafo aciclico direccionado), que tiene como nodo ra�z al torso y las relaciones entre las partes del cuerpo son modeladas en base a distribuciones Gaussianas y el aprendizaje previo basados en multiples vistas y multiples articulaciones lo cual puede ayudar a dar una mejor interpretaci�n semantica.
Esta t�cnica tiene dos procesos primero un modelo entrenado y un �rbol cinem�tico simulando las partes del cuerpo y sus posibles movimientos.