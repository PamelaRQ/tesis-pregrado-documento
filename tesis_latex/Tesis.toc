\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducción}{13}
\contentsline {section}{\numberline {1.1}Motivación y Contexto}{13}
\contentsline {section}{\numberline {1.2}Planteamiento del Problema}{14}
\contentsline {section}{\numberline {1.3}Objetivos}{14}
\contentsline {subsection}{\numberline {1.3.1}Objetivos Específicos}{14}
\contentsline {section}{\numberline {1.4}Organización de la tesis}{15}
\contentsline {chapter}{\numberline {2}Marco Teórico}{16}
\contentsline {section}{\numberline {2.1}Visión General}{16}
\contentsline {section}{\numberline {2.2}Pre-Procesamiento}{16}
\contentsline {subsection}{\numberline {2.2.1}Filtro Gaussiano}{17}
\contentsline {subsection}{\numberline {2.2.2}Dilatación}{17}
\contentsline {subsubsection}{Erosión}{18}
\contentsline {section}{\numberline {2.3}Sustracción de Fondo}{19}
\contentsline {subsection}{\numberline {2.3.1}Problemática}{19}
\contentsline {subsection}{\numberline {2.3.2}Tipos de Sustracción de Fondo}{20}
\contentsline {section}{\numberline {2.4}Técnicas de Sustracción de Fondo}{21}
\contentsline {subsection}{\numberline {2.4.1}Diferencia de frames}{22}
\contentsline {subsection}{\numberline {2.4.2}Temporal Median Filter (TMF)}{22}
\contentsline {subsubsection}{Speed Up Temporal Median Filter (SUTMF)}{22}
\contentsline {subsection}{\numberline {2.4.3}Mixture of Gaussians (MoG)}{24}
\contentsline {subsection}{\numberline {2.4.4}Optical Flow}{25}
\contentsline {section}{\numberline {2.5}Detección de Personas}{26}
\contentsline {subsection}{\numberline {2.5.1}Problemática}{27}
\contentsline {section}{\numberline {2.6}Técnicas de Detección de Personas}{27}
\contentsline {subsection}{\numberline {2.6.1}Histograms of Oriented Gradients (HOG)}{28}
\contentsline {subsection}{\numberline {2.6.2}Pictorial Structures Revisited}{30}
\contentsline {section}{\numberline {2.7}Métodos de Clasificasión}{30}
\contentsline {subsection}{\numberline {2.7.1}$k$-Nearest Neighbor - ($k$NN)}{30}
\contentsline {subsection}{\numberline {2.7.2}Support Vector Machines - SVM}{31}
\contentsline {subsubsection}{SVM Linearmente separable}{31}
\contentsline {subsubsection}{SVM No linearmente separable}{32}
\contentsline {chapter}{\numberline {3}Trabajos Relacionados}{34}
\contentsline {chapter}{\numberline {4}Detección Automática de Personas en Secuencias de Video}{38}
\contentsline {section}{\numberline {4.1}Consideraciones iniciales}{38}
\contentsline {section}{\numberline {4.2}Propuesta}{38}
\contentsline {subsection}{\numberline {4.2.1}Pre-procesamiento}{39}
\contentsline {subsection}{\numberline {4.2.2}Sustracción de fondo}{39}
\contentsline {subsection}{\numberline {4.2.3}Detección de personas}{41}
\contentsline {subsection}{\numberline {4.2.4}Clasificación}{41}
\contentsline {section}{\numberline {4.3}Consideraciones finales}{42}
\contentsline {chapter}{\numberline {5}Experimentación y Resultados}{43}
\contentsline {section}{\numberline {5.1}Bases de videos de video vigilancia}{43}
\contentsline {subsection}{\numberline {5.1.1}Base de videos propio}{43}
\contentsline {subsection}{\numberline {5.1.2}Base de videos PETS2006}{44}
\contentsline {section}{\numberline {5.2}Experimentos con Técnicas de Sustracción de Fondo}{45}
\contentsline {subsection}{\numberline {5.2.1}Comparación en eficiencia}{45}
\contentsline {subsection}{\numberline {5.2.2}Comparación por Tiempo de Respuesta}{48}
\contentsline {section}{\numberline {5.3}Experimentos con Técnicas de Detección de Personas}{49}
\contentsline {chapter}{\numberline {6}Conclusiones y Trabajos Futuros}{50}
\contentsline {chapter}{Bibliografía}{52}
